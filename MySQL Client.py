import os
import os.path
import subprocess
import time

import sublime
import sublime_plugin


SETTINGS_FILE = "MySQL Client.sublime-settings"


class RunMysqlQueryCommand(sublime_plugin.WindowCommand):

    def __init__(self, *args, **kwargs):
        sublime_plugin.WindowCommand.__init__(self, *args, **kwargs)
        self._settings = None

    def run(self):

        # Read the settings.
        settings = sublime.load_settings(SETTINGS_FILE)
        overrides = {}
        self._settings = OverrideableSettings(settings, overrides)

        # Read the selection(s) as the query. If more than one, combine them.
        query = self._get_selection()
        self._run_query(query)

    def _run_query(self, query):

        mysql = self._settings.get("mysql")
        defaults_file = self._settings.get("defaults_file")

        # Build a list of arguments for the mysql command.
        args = [mysql]

        if defaults_file:
            defaults_file = os.path.expanduser(defaults_file)
            args += ["--defaults-file=" + defaults_file]

        # TODO Add other args from settings
        args += ["--table", "-r"]

        # Convert the query into a bytes.
        query_bytes = query.encode("utf-8")
        command = subprocess.Popen(args,
                                   stdin=subprocess.PIPE,
                                   stdout=subprocess.PIPE,
                                   stderr=subprocess.PIPE)
        time_start = time.time()
        output, error = command.communicate(query_bytes)
        time_end = time.time()
        elapsed = time_end - time_start

        if output:
            output = output.decode("utf-8")
            print(output)
            print("Time: %f seconds" % elapsed)
        else:
            error = error.decode("utf-8")
            print(error)

    def _get_selection(self):
        # Return a string of the selected text or the entire buffer.
        # if there are multiple selections, concatenate them.
        view = self.window.active_view()
        sels =  view.sel()
        if len(sels) == 1 and sels[0].empty():
            selection = view.substr(sublime.Region(0, view.size()))
        else:
            selection = []
            for sel in sels:
                selection.append(view.substr(sel))
            selection = "\n".join(selection)
        return selection



class OverrideableSettings():
    """
    Class for adding a layer of overrides on top of a Settings object

    The class is read-only. If a dictionary-like _overrides member is present,
    the get() method will look there first for a setting before reading from
    the _settings member.
    """

    def __init__(self, settings=None, overrides=None):
        self._settings = settings
        self._overrides = overrides

    def set_settings(self, settings):
        self._settings = settings

    def set_overrides(self, overrides):
        self._overrides = overrides

    def get(self, setting, default=None):
        if self._overrides and setting in self._overrides:
            return self._overrides[setting]
        elif self._settings:
            return self._settings.get(setting, default)
        else:
            return default
