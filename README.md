# MySQL Sublime Client

Run MySQL queries from the Sublime.

## Requirements

You must have the mysql client installed locally to use this package.

## Setup

### Package User Settings

Open the user settings. `Sublime Text` -> `Preferences` -> `Package Settings` -> `MySQL Client` -> `Settings - User`

In the settings file, ensure that `mysql` and `defaults_file` are set propertly.

    - `mysql`: Path to MySQL client binary
    - `defaults_file`: Path to the my.cnf file for the database connection

Example:

```javascript
{
    // Path to a my.cnf file for the database connection
    "defaults_file": "~/mydatabase.my.cnf",
    // Path do the mysql client binary
    "mysql": "/usr/local/bin/mysql"
}
```

### Configuration File

Create a configuration file (my.cnf) to provide the connecton information for your database.

Example:

```ini
[client]
database=mydtabase
port=3306
host=localhost
user=root
password=mypassword
default-character-set=utf8
```

See `mysql --help` for a list of additional options.
